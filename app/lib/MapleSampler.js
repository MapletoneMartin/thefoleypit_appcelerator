/*:::::::Hyperloop includes:::::::*/
var UIKit = require('UIKit');
var AVFoundation = require('AVFoundation');
var Foundation = require('Foundation');


var AVAudioEngine = require('AVFoundation/AVAudioEngine');
var AVAudioUnitSampler = require('AVFoundation/AVAudioUnitSampler');
var AVAudioSession = require('AVFoundation/AVAudioSession');

/*::::::::::::::::Effects:::::::::::::::::
 *:::::ALL enums etc from the original::::
 * :::::Classes have been copied here:::::
 * ::::::(bottom) for easier access:::::::
 */
var AVAudioUnitReverb = require('AVFoundation/AVAudioUnitReverb');
var AVAudioUnitEQ = require('AVFoundation/AVAudioUnitEQ');
var AVAudioUnitDistortion = require('AVFoundation/AVAudioUnitDistortion');
/*:::::::Native Types:::::::*/
var Bundle = require('Foundation/NSBundle');
var NSString = require('Foundation/NSString');

/*:::::::START:::::::*/
var engine = new AVAudioEngine();
var sampler = new AVAudioUnitSampler();

var unitReverb = new AVAudioUnitReverb();
	unitReverb.loadFactoryPreset(getAVAudioUnitReverbPreset('SmallRoom'));
	unitReverb.wetDryMix = 0;

var EQNode = AVAudioUnitEQ.alloc().initWithNumberOfBands(8);

engine.attachNode(sampler); 
engine.attachNode(unitReverb);
engine.attachNode(EQNode);

engine.connectToFormat(sampler, EQNode, null);
engine.connectToFormat(EQNode, unitReverb, null);
engine.connectToFormat(unitReverb, engine.mainMixerNode, null);

exports.startEngine = function() {
	//Will this ever happen?
	if (engine.isRunning()) {
		console.log("audio engine already started");
		return;
	}

	try {
		engine.startAndReturnError();
		console.log("audio engine started");
	} catch(e) {
		console.log("oops \(error)");
		console.log("could not start audio engine: " + e);
	}
};

exports.setSessionPlayback = function() {
	var audioSession = AVAudioSession.sharedInstance();
	//Needs a proper NSString as a magic word
	var AVAudioSessionCategoryPlayback = NSString.alloc().initWithString("AVAudioSessionCategoryPlayback");
	var AVAudioSessionCategoryOptionMixWithOthers = require('AVFoundation').AVAudioSessionCategoryOptionMixWithOthers;
	//console.log('AVAudioSessionCategoryPlayback String content: ' + AVAudioSessionCategoryPlayback + " AVAudioSessionCategoryOptionMixWithOthers string content: " + AVAudioSessionCategoryOptionMixWithOthers);
	try {
		audioSession.setCategoryWithOptionsError(AVAudioSessionCategoryPlayback, AVAudioSessionCategoryOptionMixWithOthers);
	} catch (e) {
		console.log("couldn't set category: " + e);
		return;
	}

	try {
		audioSession.setActiveError(1);
		console.log('AudioSession was set active');
	} catch(e) {
		console.log("couldn't set category active \(error)" + e);
		return;
	}
};

exports.play = function(note, tuning) {
	//The globalTuning unit is cents. The default value is 0.0. 
	//The range of valid values is -2400 to 2400 cents.
	if(!tuning){
		tuning = 0;
	};
	sampler.globalTuning = tuning;
	console.log("playing note " + note + ' with tuning ' + tuning);
	
	sampler.startNoteWithVelocityOnChannel(note, 46, 0);
};

exports.stop = function() {
	sampler.stopNoteOnChannel(60, 0);
};


exports.loadSamples = function(soundFiles, folder, soundFileType) {
	
	//Pass array of wavs (soundFileType="wav") or aupreset (soundFileType="aupreset") 
	try {
		
		var mainBundle = Bundle.mainBundle;
		console.log('Adding: ' + soundFileType + ' to sampler: ' + mainBundle.URLForResourceWithExtensionSubdirectory(soundFiles, soundFileType, folder));
		try {
				
			if (soundFileType === "aupreset"){
				sampler.loadInstrumentAtURLError(mainBundle.URLForResourceWithExtensionSubdirectory(soundFiles, soundFileType, folder));
				console.log("loaded aupreset samples");
				
			} else if (soundFileType ==="wavs"){
				
				sampler.loadInstrumentAtURLError(mainBundle.URLsForResourcesWithExtensionSubdirectory("wav", folder));
				console.log("loaded wavs samples");
				
			} else {
				
				alert('wrong soundFileType passed to sampler, use "wavs" or "aupreset" as last argument on loadSamples');
			}
			
		} catch(e) {
			console.log('Error loading samples::: ' + e);
		}
	} catch(e) {

		console.log("Error trying to locate the bundle::: " + e);
	}
};
//:::::::::::Effect settings:::::::::
//:::::::::::::::REVERB::::::::::::::
function getAVAudioUnitReverbPreset(reverb){
		
	switch (reverb) {
	case 'SmallRoom':
		return 0;
		break;
	case 'MediumRoom':
		return 1;
		break;
	case 'LargeRoom':
		return 2;
		break;
	case 'MediumHall':
		return 3;
		break;
	case 'LargeHall':
		return 4;
		break;
	case 'Plate':
		return 5;
		break;
	case 'MediumChamber':
		return 6;
		break;
	case 'LargeChamber':
		return 7;
		break;
	case 'Cathedral':
		return 8;
		break;
	case 'LargeRoom2':
		return 9;
		break;
	case 'MediumHall2':
		return 10;
		break;
	case 'MediumHall3':
		return 11;
		break;
	case 'LargeHall2':
		return 12;
		break;
	default:
		return 0;
	}
}

exports.reverbList = ['SmallRoom', 'MediumRoom', 'LargeRoom', 'MediumHall', 'LargeHall',
					 	'Plate', 'MediumChamber', 'LargeChamber', 'Cathedral', 'LargeRoom2',
					  	'MediumHall2', 'MediumHall3', 'LargeHall2'
					 ];

exports.setReverb = function(reverbPreset, wetDryMix){
	if(!reverbPreset || reverbPreset === undefined){
		reverbPreset = 'SmallRoom';
	};
	if(!wetDryMix){
		wetDryMix = 0;
	};
	unitReverb.loadFactoryPreset(getAVAudioUnitReverbPreset(reverbPreset));
	unitReverb.wetDryMix = wetDryMix;
};

//:::::::::::::::EQUALIZER::::::::::::::

exports.EQList = ['Parametric', 'LowPass', 'HighPass', 'ResonantLowPass', 'ResonantHighPass', 'BandPass', 
					'BandStop', 'LowShelf', 'HighShelf', 'ResonantLowShelf', 'ResonantHighShelf'
				 ];
					 
exports.getAVAudioUnitEQFilterType = function getAVAudioUnitEQFilterType(filterType) {
/*
       Depending on the filter type, a combination of one or all of the filter parameters defined 
        in AVAudioUnitEQFilterParameters are used to set the filter.
     
        AVAudioUnitEQFilterTypeParametric
            Parametric filter based on Butterworth analog prototype.
            Required parameters: frequency (center), bandwidth, gain
     
        AVAudioUnitEQFilterTypeLowPass
            Simple Butterworth 2nd order low pass filter
            Required parameters: frequency (-3 dB cutoff at specified frequency)
        
        AVAudioUnitEQFilterTypeHighPass
            Simple Butterworth 2nd order high pass filter
            Required parameters: frequency (-3 dB cutoff at specified frequency)
     
        AVAudioUnitEQFilterTypeResonantLowPass
            Low pass filter with resonance support (via bandwidth parameter)
            Required parameters: frequency (-3 dB cutoff at specified frequency), bandwidth
     
        AVAudioUnitEQFilterTypeResonantHighPass
            High pass filter with resonance support (via bandwidth parameter)
            Required parameters: frequency (-3 dB cutoff at specified frequency), bandwidth
     
        AVAudioUnitEQFilterTypeBandPass
            Band pass filter
            Required parameters: frequency (center), bandwidth
     
        AVAudioUnitEQFilterTypeBandStop
            Band stop filter (aka "notch filter")
            Required parameters: frequency (center), bandwidth
     
        AVAudioUnitEQFilterTypeLowShelf
            Low shelf filter
            Required parameters: frequency (center), gain
     
        AVAudioUnitEQFilterTypeHighShelf
            High shelf filter
            Required parameters: frequency (center), gain
     
        AVAudioUnitEQFilterTypeResonantLowShelf
            Low shelf filter with resonance support (via bandwidth parameter)
            Required parameters: frequency (center), bandwidth, gain
     
        AVAudioUnitEQFilterTypeResonantHighShelf
            High shelf filter with resonance support (via bandwidth parameter)
            Required parameters: frequency (center), bandwidth, gain
*/
	switch (filterType) {
	case 'Parametric':
		return 0;
		break;
	case 'LowPass':
		return 1;
		break;
	case 'HighPass':
		return 2;
		break;
	case 'ResonantLowPass':
		return 3;
		break;
	case 'ResonantHighPass':
		return 4;
		break;
	case 'BandPass':
		return 5;
		break;
	case 'BandStop':
		return 6;
		break;
	case 'LowShelf':
		return 7;
		break;
	case 'HighShelf':
		return 8;
		break;
	case 'ResonantLowShelf':
		return 9;
		break;
	case 'ResonantHighShelf':
		return 10;
		break;
	}
};


exports.setEq = function(band, thisFilterType, frequency, bandwidth, bypass, gain) {

			// Default:    AVAudioUnitEQFilterTypeParametric
			// Use getAVAudioUnitEQFilterType() to get the enumeration for the name
		EQNode.bands.objectAtIndex(band).setValueForKey(thisFilterType, 'filterType');
		
			// Range:      20 -> (SampleRate/2)
   			// Unit:       Hertz
		EQNode.bands.objectAtIndex(band).setValueForKey(frequency, 'frequency');
		
			// Range:      0.05 -> 5.0
    		// Unit:       Octaves
		EQNode.bands.objectAtIndex(band).setValueForKey(bandwidth, 'bandwidth');
		
			// Default:    YES
		EQNode.bands.objectAtIndex(band).setValueForKey(bypass, 'bypass');
		
			// Range:      -96 -> 24
		    // Default:    0
		    // Unit:       dB
		EQNode.bands.objectAtIndex(band).setValueForKey(gain, 'gain');
		console.log('Band ' + band + 'filtertype is : ' + EQNode.bands.objectAtIndex(band).valueForKey('filterType'));
		console.log('Band ' + band + 'frequency is : ' + EQNode.bands.objectAtIndex(band).valueForKey('frequency'));
		console.log('Band ' + band + 'Bandwidth is : ' + EQNode.bands.objectAtIndex(band).valueForKey('bandwidth'));
		console.log('Band ' + band + 'Bypass is : ' + EQNode.bands.objectAtIndex(band).valueForKey('bypass'));
		console.log('Band ' + band + 'Gain is : ' + EQNode.bands.objectAtIndex(band).valueForKey('gain'));

}; 

//::::::::::::Helpers::::::::::::::::

var pseudoRandomInt = 0;
exports.getRandomInt = function (min, max) {
	randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
	if (randomInt !== pseudoRandomInt){
		pseudoRandomInt = randomInt;
		return randomInt;
	} else {
		return 60;
	}
};
