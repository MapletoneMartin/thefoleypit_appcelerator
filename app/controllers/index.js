var MapleSampler = require('MapleSampler');
//Temp global get rid of it later =)
var started = 0;

//This Sequence should be in the docs.
try {
	MapleSampler.loadSamples("stepsWood1_all", "sounds", "aupreset");
} catch(e) {
	console.log('Something went wrong while loading samples: ' + e);	
}

MapleSampler.startEngine();
MapleSampler.setSessionPlayback();
MapleSampler.setReverb('smallRoom', 0);
getReverbPresets();
turnView($.eqView, 270);
$.index.open();

function turnView(thisView, degrees) {
	var matrix = Ti.UI.create2DMatrix();
	matrix = matrix.rotate(degrees);
	thisView.transform = matrix;
}

function playRandomSound(trigger, downNote) {
	if(!trigger){
		//Plays random sound and pitches each step randomly
		MapleSampler.play(MapleSampler.getRandomInt(60, 80), MapleSampler.getRandomInt(-100, 100));
		//Just checking for memory leaks old school style =) Remove later
		console.log(Titanium.Platform.getAvailableMemory());
	} else if (trigger === 'down'){
		var randomNote = MapleSampler.getRandomInt(12, 31);
		//Plays random sound and pitches each step randomly
		MapleSampler.play(randomNote, MapleSampler.getRandomInt(-100, 100));
		//Just checking for memory leaks old school style =) Remove later
		console.log(Titanium.Platform.getAvailableMemory());
		return randomNote;
	} else if(trigger === 'up'){
		//Plays random sound and pitches each step randomly
		MapleSampler.play(downNote + 24, MapleSampler.getRandomInt(-100, 100));
		//Just checking for memory leaks old school style =) Remove later
		console.log('should trigger an up-sample');
	}
	
};

function playRandomScuff() {
	console.log('scuff');
	MapleSampler.stop();
}

function changePatch() {
	if (started === 1) {
		MapleSampler.loadSamples("stepsWood", "sounds", "aupreset");
		started = 0;
	} else {
		MapleSampler.loadSamples("grass", "sounds", "aupreset");
		started = 1;
	}

}

function reverbAmountChanged(e) {
	MapleSampler.setReverb($.reverbs.views[$.reverbs.currentPage].preset, e.value);
}

function getReverbPresets() {
	var reverbViews = [];
	console.log(MapleSampler.reverbList);
	_.each(MapleSampler.reverbList, function(someThing) {

		var myView = Ti.UI.createView({
			backgroundColor : ('00000' + (Math.random() * (1 << 24) | 0).toString(16)).slice(-6)
		});

		var aLabel = Ti.UI.createLabel({
			text : someThing,
			color : 'white',
			textAlign : 'center'
		});

		// Add to the parent view.
		myView.add(aLabel);
		myView.preset = someThing;
		reverbViews.push(myView);

	});
	//console.log(reverbViews);
	$.reverbs.setViews(reverbViews);
	$.reverbs.scrollToView(1);
}

function changeReverbPatch(e) {
	console.log(e.view.preset);
	MapleSampler.setReverb(e.view.preset, $.reverbSlider.value);
}

function eqChanged(e) {
	//console.log(MapleSampler.EQList);
	console.log(JSON.stringify(e));
	if (e.value === false) {
		var bypassed = 1;
	} else {
		var bypassed = 0;
	};

	MapleSampler.setEq(e.source.band, MapleSampler.getAVAudioUnitEQFilterType(e.source.filterType), e.source.frequency, e.source.bandwidth, bypassed, e.value);
}

function setInitialEQstate() {
	$.eq1.setIndex = 0;
	MapleSampler.setEq(1, MapleSampler.getAVAudioUnitEQFilterType('HighPass'), 80, 1, 1, 0);
	MapleSampler.setEq(7, MapleSampler.getAVAudioUnitEQFilterType('LowPass'), 12000, 1, 1, 0);
}

function eqLowHighPassHandler(e) {
	console.log(e);
	if (e.index === 1) {
		MapleSampler.setEq(e.source.band, MapleSampler.getAVAudioUnitEQFilterType(e.source.filterType), e.source.frequency, e.source.bandwidth, 0, 0);
	} else {
		MapleSampler.setEq(e.source.band, MapleSampler.getAVAudioUnitEQFilterType(e.source.filterType), e.source.frequency, e.source.bandwidth, 1, 0);
	}
}


//****************************EVENT LISTENERS**************************************

$.thePit.addEventListener('touchstart', function(e){
	var randomNote = playRandomSound('down');
	
	//We might need to check for how long the user presses etc.
	$.thePit.addEventListener('touchend', released);
	
	function released(e){
		playRandomSound('up', randomNote);
		$.thePit.removeEventListener('touchend', released);
	};
});

$.thePit.addEventListener('touchmove', function(e){
	console.log('we should do something cool here');
});


