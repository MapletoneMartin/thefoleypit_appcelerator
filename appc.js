module.exports = {
	hyperloop: {
		ios: {
			xcodebuild: {
				flags: {
					
				},
				frameworks: [
					'AVFoundation',
					'UIKit'
				]
			},
			thirdparty: {
				'MyFramework': {
					source: ['src'],
					header: 'src',
					resource: 'src'
				}
			}
		}
	}
};